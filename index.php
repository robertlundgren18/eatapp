<?php
require_once 'src/pages/Top.php';
/** @var \EatApp\Database\Database $database */
$database = new EatApp\Database\Database($app);
/** @var \EatApp\Mapper\DataMapper $meals */
$meals = new EatApp\Mapper\Meals($database);
?>

<a href="create.php" class="btn btn-success btn-block mt-4">Skapa</a>

<table class="table mt-5">
    <thead>
        <tr>
            <th scope="col">Namn</th>
            <th scope="col">Beskrivning</th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $data = $meals->findAll();

        foreach ($data as $meal) {
            echo '
            <tr>
                <th scope="row">'. $meal->getName() .'</th>
                <td>'. $meal->getDescription() .'</td>
                <td>
                    <a href="view.php?id='. $meal->getId() .'" class="btn btn-info btn-block">Läs mer</a>
                </td>
            </tr>
            ';
        }
        ?>
    </tbody>
</table>

<?php
require_once 'src/pages/Bot.php';
?>