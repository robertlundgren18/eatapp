<?php
require_once 'src/pages/Top.php';
/** @var \EatApp\Database\Database $database */
$database = new EatApp\Database\Database($app);
$request = new \EatApp\Component\HTTP\Request\Request();

try {
    $id = $request->get('id');
} catch (\EatApp\Component\HTTP\Request\Exceptions\InvalidQueryKey $e) {
    header('Location: index.php');
    exit;
}

$meals = new \EatApp\Mapper\Meals($database);
$meal = $meals->find($id);

if (!$meal) {
    header('Location: index.php');
    exit;
}

$ingredientsMapper = new \EatApp\Mapper\Ingredients($database);
$ingredients = $ingredientsMapper->findByMealId($meal->getId());

$counter = 0;
?>
<script>
let number = 0;
function addIngredient()
{
    let ingredients = document.getElementById('ingredients');
    let elements = buildForm();
    ingredients.innerHTML += elements;
}

function buildForm()
{
    let x = '<div class="form-group col-sm-6"><label for="iName' + number + '">Namn:</label><input type="text" name="iName_'+ number +'" id="iName'+ number +'" class="form-control" placeholder="Namn"></div>';
    let y = '<div class="form-group col-sm-6"><label for="amount">Mängd:</label><input type="text" name="amount_'+ number +'" id="amount'+ number +'" class="form-control" placeholder="Mängd"></div>';
    number++;
    return [x + y];
}
</script>
<a href="index.php" class="btn btn-success btn-block mt-4">Tillbaka</a>
<form action="process/update.php" method="post" id="createMeal" class="mt-3">
    <input type="hidden" name="id" value="<?= $meal->getId(); ?>" required>
    <div class="form-group">
        <label for="mealName">Maträtt namn:</label>
        <input type="text" name="name" id="mealName" class="form-control" placeholder="Namn" value="<?= $meal->getName(); ?>" requried>
    </div>
    <div class="form-group">
        <label for="mealDescription">Beskrivning:</label>
        <textarea name="description" id="mealDescription" class="form-control" placeholder="Beskrivning" rows="5" required><?= $meal->getDescription(); ?></textarea>
    </div>
    <div class="row" id="ingredients">
        <?php
        foreach ($ingredients as $ingredient) {
            echo '<div class="form-group col-sm-6"><label for="iName'. $counter .'">Namn:</label><input type="text" name="iName_'. $counter .'_'. $ingredient->getId() .'" id="iName'. $counter .'" class="form-control" placeholder="Namn" value="' . $ingredient->getName() .'"></div>';
            echo '<div class="form-group col-sm-6"><label for="amount">Mängd:</label><input type="text" name="amount_'. $counter .'_'. $ingredient->getId() .'" id="amount'. $counter .'" class="form-control" placeholder="Mängd" value="'. $ingredient->getAmount() .'"></div>';
            echo '<script>number++</script>';
            $counter++;
        }
        ?>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <button type="button" class="btn btn-secondary btn-block" onclick="addIngredient();">Lägg till en ingrediens</button>
        </div>
        <div class="col-sm-6">
            <button type="submit" class="btn btn-primary btn-block" onclick="submit()">Updpatera</button>
        </div>
    </div>
</form>

<script>
function submit()
{
    doument.getElementById('createMeal').submit();
}
</script>

<?php
require_once 'src/pages/Bot.php';
?>