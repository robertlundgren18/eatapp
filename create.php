<?php
require_once 'src/pages/Top.php';
/** @var \EatApp\Database\Database $database */
$database = new EatApp\Database\Database($app);
?>

<a href="index.php" class="btn btn-success btn-block mt-4">Tillbaka</a>
<form action="process/new.php" method="post" id="createMeal" class="mt-3">
    <div class="form-group">
        <label for="mealName">Maträtt namn:</label>
        <input type="text" name="name" id="mealName" class="form-control" placeholder="Namn" requried>
    </div>
    <div class="form-group">
        <label for="mealDescription">Beskrivning:</label>
        <textarea name="description" id="mealDescription" class="form-control" placeholder="Beskrivning" rows="5" required></textarea>
    </div>
    <div class="row" id="ingredients">
        <div class="form-group col-sm-6">
            <label for="iName">Namn:</label>
            <input type="text" name="iName_0" id="iName" class="form-control" placeholder="Namn">
        </div>
        <div class="form-group col-sm-6">
            <label for="amount">Mängd:</label>
            <input type="text" name="amount_0" id="amount" class="form-control" placeholder="Mängd">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <button type="button" class="btn btn-secondary btn-block" onclick="addIngredient();">Lägg till en ingrediens</button>
        </div>
        <div class="col-sm-6">
            <button type="submit" class="btn btn-primary btn-block" onclick="submit()">Skapa</button>
        </div>
    </div>
</form>

<script>
let number = 1;
function addIngredient()
{
    let ingredients = document.getElementById('ingredients');
    let elements = buildForm();
    ingredients.innerHTML += elements;
}

function buildForm()
{
    let x = '<div class="form-group col-sm-6"><label for="iName' + number + '">Namn:</label><input type="text" name="iName_'+ number +'" id="iName'+ number +'" class="form-control" placeholder="Namn"></div>';
    let y = '<div class="form-group col-sm-6"><label for="amount">Mängd:</label><input type="text" name="amount_'+ number +'" id="amount'+ number +'" class="form-control" placeholder="Mängd"></div>';
    number++;
    return [x + y];
}

function submit()
{
    doument.getElementById('createMeal').submit();
}
</script>

<?php
require_once 'src/pages/Bot.php';
?>