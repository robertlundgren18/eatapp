<?php
require __DIR__ . '/../src/Bootstrap.php';

$location = __DIR__ . '/../edit.php';

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('Location: '. $location);
    exit();
}

try {
    $id = $request->post('id');
} catch (\EatApp\Component\HTTP\Request\Exceptions\InvalidQueryKey $e) {
    header('Location: index.php');
    exit;
}

$fail = false;

if (empty($request->post('name'))) {
    $fail = true;
} elseif (empty($request->post('description'))) {
    $fail = true;
}

if ($fail) {
    header('Location: '. $location);
    exit;
}

$database = new \EatApp\Database\Database(new \EatApp\App());
$mapper = new \EatApp\Mapper\Meals($database);
$meal = $mapper->find($id);
$meal
    ->setName($request->post('name'))
    ->setDescription($request->post('description'))
    ->save();
/** @var \EatApp\Mapper\Ingredients $mapper */
$mapper = new \EatApp\Mapper\Ingredients($database);

foreach ($request->getPost() as $key => $value) {
    if (strpos(strtolower($key), 'iname_') !== false) {
        $string = explode('_', $key);
        $numberOfIngredient = $string[1];

        $ingredient = $mapper->find((int)$string[2]);

        $ingredient
            ->setMealId($meal->getId())
            ->setName($value)
            ->setAmount($request->getPost()['amount_' . $string[1] . '_' . $string[2]])
            ->save();
    }
}

header('Location: ../index.php');
exit;