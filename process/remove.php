<?php
require __DIR__ . '/../src/Bootstrap.php';

$location = __DIR__ . '/../index.php';

try {
    $id = $request->get('id');
} catch (\EatApp\Component\HTTP\Request\Exceptions\InvalidQueryKey $e) {
    header('Location: index.php');
    exit;
}

$database = new \EatApp\Database\Database(new \EatApp\App());
$mapper = new \EatApp\Mapper\Meals($database);
$meal = $mapper->find($id);
$meal->remove();
/** @var \EatApp\Mapper\Ingredients $mapper */
$mapper = new \EatApp\Mapper\Ingredients($database);
$ingredients = $mapper->findByMealId($id);

foreach ($ingredients as $ingredient) {
    $ingredient->remove();
}

header('Location: ../index.php');
exit;