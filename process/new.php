<?php
require __DIR__ . '/../src/Bootstrap.php';

$location = __DIR__ . '/../create.php';

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('Location: '. $location);
    exit();
}

$fail = false;

if (empty($request->post('name'))) {
    $fail = true;
} elseif (empty($request->post('description'))) {
    $fail = true;
} elseif (empty($request->post('iName_0'))) {
    $fail = true;
} elseif (empty($request->post('amount_0'))) {
    $fail = true;
}

if ($fail) {
    header('Location: '. $location);
    exit;
}

$database = new \EatApp\Database\Database(new \EatApp\App());
$mapper = new \EatApp\Mapper\Meals($database);
$meal = new \EatApp\Entities\Meal($mapper);
/** @var \PDO $connection */
$connection = $database->connection();
$meal
    ->setName($request->post('name'))
    ->setDescription($request->post('description'))
    ->save();
$meal->setId($connection->lastInsertId());
/** @var \EatApp\Mapper\Ingredients $mapper */
$mapper = new \EatApp\Mapper\Ingredients($database);
/** @var \EatApp\Entities\Ingredient $ingredient */
$ingredient = new \EatApp\Entities\Ingredient($mapper);

foreach ($request->getPost() as $key => $value) {
    if (strpos(strtolower($key), 'iname_') !== false) {
        $string = explode('_', $key);
        $numberOfIngredient = $string[1];

        $ingredient
            ->setMealId($meal->getId())
            ->setName($value)
            ->setAmount($request->getPost()['amount_' . $numberOfIngredient])
            ->save();
    }
}

header('Location: ../index.php');
exit;