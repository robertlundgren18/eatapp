<?php
require 'src/Bootstrap.php';

$app = new \EatApp\App();
?>
<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title><?= $app->getEnvironment()['APP_NAME']; ?></title>

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    </head>
    <body>
        <h3 class="text-center mt-3 display-3">EatApp</h3>
        <div class="container">