<?php
namespace EatApp\Component\HTTP\Request;

class Request
{
    /** @var array $request */
    private $request;

    /** 
     * @return void
     */
    public function __construct()
    {
        $this->request['get']   = $_GET;
        $this->request['post']  = $_POST;
    }

    /**
     * @return string|void
     */
    public function fetchUrl()
    {
        if ($this->pathExists($this->request)) {
            return $this->formatUrl($this->request['get']['path']);
        }
    }

    /**
     * @param string $key
     *
     * @throws \EatApp\Component\HTTP\Request\Exceptions\InvalidQueryKey
     *
     * @return mixed
     */
    public function get(string $key) 
    {
        if (isset($this->request['get'][$key])) {
            return $this->request['get'][$key];
        }

        throw new \EatApp\Component\HTTP\Request\Exceptions\InvalidQueryKey('Invalid query key.');
    }

    /**
     * @param string $key
     *
     * @throws \EatApp\Component\HTTP\Request\Exceptions\InvalidQueryKey
     *
     * @return mixed
     */
    public function post(string $key)
    {
        if (isset($this->request['post'][$key])) {
            return $this->request['post'][$key];
        }

        throw new \EatApp\Component\HTTP\Request\Exceptions\InvalidQueryKey('Invalid query key. Key: ' . $key);
    }

    /**
     * @return array
     */
    public function getPost()
    {
        return $this->request['post'];
    }

    /**
     * @param string $parameterName
     *
     * @return bool
     */
    public function parameterExists(string $parameterName) : bool
    {
        return (isset($this->request['get'][$parameterName]));
    }

    /**
     * @param string $url
     *
     * @return string
     */
    protected function formatUrl(string $url) : string
    {
        if (strpos(strtolower($url), 'robert') !== false) {
            $url = str_replace('/robert', '', $url);
        }

        return $url;
    }

    /**
     * @param array $request
     * 
     * @return bool
     */
    protected function pathExists(array $request) : bool
    {
        return (isset($this->request['get']['path']));
    }
}