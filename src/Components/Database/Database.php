<?php
namespace EatApp\Database;

class Database
{
    /** @var \EatApp\Database\DatabaseCore $core */
    private $core;

    /**
     * @param \EatApp\App $app
     *
     * @return void
     */
    public function __construct(\EatApp\App $app)
    {
        $this->core = new \EatApp\Database\DatabaseCore($app);
    }

    /**
     * @return \PDO
     */
    public function connection()
    {
        return $this->core->create();
    }
}