<?php
namespace EatApp\Database;

class DatabaseCore
{
    /** @var \EatApp\App $app */
    private $app;
    /** @var \PDO $connection */
    protected static $connection = null;

    public function __construct(\EatApp\App $app)
    {
        $this->app = $app;
    }

    /**
     * Creates and returns a PDO object
     *
     * @return \PDO
     */
    public function create()
    {
        if (isset(self::$connection)) {
            return self::$connection;
        }

        $dsn = 'mysql:';

        foreach ($this->app->getEnvironment()['DB']['DSN'] as $key => $value) {
            if ($key == 'NAME') {
                $key = 'dbname';
            }
            $dsn .= strtolower($key) . '=' . $value . ';';
        }

        self::$connection = new \PDO(
            $dsn,
            $this->app->getEnvironment()['DB']['USER'],
            $this->app->getEnvironment()['DB']['PASS'],
            [
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            ]
        );

        foreach ($this->app->getEnvironment()['DB']['ATTRIBUTES'] as $key => $value) {
            self::$connection->setAttribute(constant('\PDO::' . $key), constant('\PDO::' . $value));
        }
    
        return self::$connection;
    }

    public static function __callStatic($name, $args)
    {
        return call_user_func_array([
            $this->create(),
            $name,
        ]);
    }
}