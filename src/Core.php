<?php
namespace EatApp;

class Core
{
    /** @var \Dotenv\Dotenv $env */
    private $env;

    /**
     * Setting up all the environment variables
     *
     * @return void
     */
    public function __construct()
    {
        $this->env = \Dotenv\Dotenv::create(__DIR__ . '/..');
        $this->env->load();
        $this->env->data = [
            'APP_NAME' => getenv('APP_NAME'),
            'APP_URL' => getenv('APP_URl'),
            'DB' => [
                'USER' => getenv('DB_USER'),
                'PASS' => getenv('DB_PASS'),
                'DSN' => [
                    'HOST' => getenv('DB_HOST'),
                    'PORT' => getenv('DB_PORT'),
                    'NAME' => getenv('DB_NAME'),
                ],
                'ATTRIBUTES' => [
                    'ATTR_ERRMODE' => getenv('MYSQL_ERRMODE')
                ]
            ],
        ];
    }

    /**
     * @return array
     */
    public function getEnvironment()
    {
        return $this->env->data;
    }
}