<?php
namespace EatApp\Mapper;

class DataMapper
{
    /** @var \EatApp\Database\Database $connection */
    private $connection;

    public function __construct(\EatApp\Database\Database $connection)
    {
        $this->connection = $connection->connection();
    }

    /**
     * @return \EatApp\Database\Database
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param \EatApp\Database\Database $connection
     * 
     * @return \EatApp\Mapper\DataMapper
     */
    public function setConnection(\EatApp\Database\Database $connection)
    {
        $this->connection = $connection;
        return $this;
    }
}