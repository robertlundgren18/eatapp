<?php
namespace EatApp\Mapper;

use EatApp\Entities\Meal;

class Meals extends \EatApp\Mapper\DataMapper
{
    /**
     * @param int $id
     *
     * @return \EatApp\Entities\Meal|bool
     */
    public function find(int $id)
    {
        $fetch = "SELECT * FROM meals WHERE id = :id;";
        $stmt = $this->getConnection()->prepare($fetch);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchObject(Meal::class, [$this]);
    }

    /**
     * @return \EatApp\Entities\Meal[]
     */
    public function findAll()
    {
        $fetch = "SELECT * FROM meals;";
        return $this->getConnection()
            ->query($fetch)
            ->fetchAll(\PDO::FETCH_CLASS, Meal::class, [$this]);
    }

    /**
     * @param string $name
     *
     * @return \EatApp\Entities\Meal|bool
     */
    public function findByName(string $name) 
    {
        $fetch = "SELECT * FROM meals WHERE name = :name;";
        $stmt = $this->getConnection()->prepare($fetch);
        $stmt->bindParam(':name', $name);
        $stmt->execute();
        return $stmt->fetchObject(Meal::class, [$this]);
    }

    /**
     * @param \EatApp\Entities\Meal $meal
     *
     * @return void
     */
    public function save(\EatApp\Entities\Meal $meal)
    {
        if ($meal->isNew()) {
            $this->createNewMeal($meal);
        } else {
            $this->updateMeal($meal);
        }
    }

    /**
     * @param \EatApp\Entities\Meal $meal
     *
     * @return void
     */
    public function remove(\EatApp\Entities\Meal $meal)
    {
        $delete = "DELETE FROM meals WHERE id = :id;";
        $stmt = $this->getConnection()->prepare($delete);
        $stmt->bindValue(':id', $meal->getId());
        $stmt->execute();
    }

    /**
     * @param \EatApp\Entities\Meal $meal
     *
     * @return void
     */
    private function createNewMeal(\EatApp\Entities\Meal $meal)
    {
        $insert = "INSERT INTO meals (name, description) VALUES (:name, :description);";
        $stmt = $this->getConnection()->prepare($insert);
        $stmt->bindValue(':name', $meal->getName());
        $stmt->bindValue(':description', $meal->getDescription());
        $stmt->execute();
    }

    /**
     * @param \EatApp\Entities\Meal $meal
     *
     * @return void
     */
    private function updateMeal(\EatApp\Entities\Meal $meal)
    {
        $update = "UPDATE meals SET name=:name, description=:description WHERE id = :id;";
        $stmt = $this->getConnection()->prepare($update);
        $stmt->bindValue(':name', $meal->getName());
        $stmt->bindValue(':description', $meal->getDescription());
        $stmt->bindValue(':id', $meal->getId());
        $stmt->execute();
    }
}