<?php
namespace EatApp\Mapper;

use EatApp\Entities\Ingredient;

class Ingredients extends \EatApp\Mapper\DataMapper
{
    /**
     * @param int $id
     *
     * @return \EatApp\Entities\Ingredient|bool
     */
    public function find(int $id)
    {
        $fetch = "SELECT * FROM ingredients WHERE id = :id;";
        $stmt = $this->getConnection()->prepare($fetch);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchObject(Ingredient::class, [$this]);
    }

    /**
     * @param int $id
     *
     * @return \EatApp\Entities\Ingredient[]
     */
    public function findByMealId(int $id)
    {
        $fetch = "SELECT * FROM ingredients WHERE mealID = :id;";
        $stmt = $this->getConnection()->prepare($fetch);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_CLASS, Ingredient::class, [$this]);
    }

    /**
     * @param string $name
     *
     * @return \EatApp\Entities\Ingredient|bool
     */
    public function findByName(string $name)
    {
        $fetch = "SELECT * FROM ingredients WHERE name = :name;";
        $stmt = $this->getConnection()->prepare($fetch);
        $stmt->bindParam(':name', $name);
        $stmt->execute();
        return $stmt->fetchObject(Ingredient::class, [$this]);
    }

    /**
     * @param \EatApp\Entities\Ingredient $ingredient
     *
     * @return void
     */
    public function save(\EatApp\Entities\Ingredient $ingredient)
    {
        if ($ingredient->isNew()) {
            $this->createNewIngredient($ingredient);
        } else {
            $this->updateIngredient($ingredient);
        }
    }

    /**
     * @param \EatApp\Entities\Ingredient $ingredient
     * 
     * @return void
     */
    public function remove(\EatApp\Entities\Ingredient $ingredient)
    {
        $delete = "DELETE FROM ingredients WHERE id = :id;";
        $stmt = $this->getConnection()->prepare($delete);
        $stmt->bindValue(':id', $ingredient->getId());
        $stmt->execute();
    }

    /**
     * @param \EatApp\Entities\Ingredient $ingredient
     *
     * @return void
     */
    private function createNewIngredient(\EatApp\Entities\Ingredient $ingredient)
    {
        $create = "INSERT INTO ingredients (mealID, name, amount) VALUES (:meal, :name, :amount);";
        $stmt = $this->getConnection()->prepare($create);
        $stmt->bindValue(':meal', $ingredient->getMealId());
        $stmt->bindValue(':name', $ingredient->getName());
        $stmt->bindValue(':amount', $ingredient->getAmount());
        $stmt->execute();
    }

    /**
     * @param \EatApp\Entities\Ingredient $ingredient
     *
     * @return void
     */
    private function updateIngredient(\EatApp\Entities\Ingredient $ingredient)
    {
        $update = "UPDATE ingredients SET mealID=:meal, name=:name, amount=:amount WHERE id = :id;";
        $stmt = $this->getConnection()->prepare($update);
        $stmt->bindvalue(':meal', $ingredient->getMealId());
        $stmt->bindValue(':name', $ingredient->getName());
        $stmt->bindValue(':amount', $ingredient->getAmount());
        $stmt->bindValue(':id', $ingredient->getId());
        $stmt->execute();
    }
}