<?php
namespace EatApp\Entities;

class Ingredient extends \EatApp\Entities\Entity
{
    /** @var int $id */
    private $id;
    /** @var int $mealID */
    private $mealID;
    /** @var string $name */
    private $name;
    /** @var string $amount */
    private $amount;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return \EatApp\Entities\Ingredient
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getMealId() : int
    {
        return $this->mealID;
    }

    /**
     * @param int $id
     *
     * @return \EatApp\Entities\Ingredient
     */
    public function setMealId(int $id)
    {
        $this->mealID = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return \EatApp\Entities\Ingredient
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAmount() : string
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     *
     * @return \EatApp\Entities\Ingredient
     */
    public function setAmount(string $amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNew() : bool
    {
        return (!isset($this->id) && empty($this->id));
    }

    /**
     * @return void
     */
    public function save()
    {
        $this->getMapper()->save($this);
    }

    public function remove()
    {
        $this->getMapper()->remove($this);
    }
}