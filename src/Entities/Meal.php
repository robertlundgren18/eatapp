<?php
namespace EatApp\Entities;

class Meal extends \EatApp\Entities\Entity
{
    /** @var int $id */
    private $id;
    /** @var string $name */
    private $name;
    /** @var string $description */
    private $description;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return \EatApp\Entities\Meal
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return \EatApp\Entities\Meal
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return \EatApp\Entities\Meal
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNew() : bool
    {
        return (!isset($this->id) && empty($this->id));
    }

    /**
     * @return void
     */
    public function save()
    {
        $this->getMapper()->save($this);
    }

    public function remove()
    {
        $this->getMapper()->remove($this);
    }
}