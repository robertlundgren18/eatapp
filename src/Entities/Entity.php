<?php
namespace EatApp\Entities;

class Entity
{
    /** @var \EatApp\Mapper\DataMapper */
    private $mapper;

    public function __construct(\EatApp\Mapper\DataMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @return \EatApp\Mapper\DataMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }

    /**
     * @param \EatApp\Mapper\DataMapper $mapper
     * 
     * @return \EatApp\Entities\Entity
     */
    public function setMapper(\EatApp\Mapper\DataMapper $mapper)
    {
        $this->mapper = $mapper;
        return $this;
    }
}