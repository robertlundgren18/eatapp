<?php
require_once 'src/pages/Top.php';
/** @var \EatApp\Database\Database $database */
$database = new EatApp\Database\Database($app);
$request = new \EatApp\Component\HTTP\Request\Request();

try {
    $id = $request->get('id');
} catch (\EatApp\Component\HTTP\Request\Exceptions\InvalidQueryKey $e) {
    header('Location: index.php');
    exit;
}

$meals = new \EatApp\Mapper\Meals($database);
$meal = $meals->find($id);

if (!$meal) {
    header('Location: index.php');
    exit;
}

$ingredientsMapper = new \EatApp\Mapper\Ingredients($database);
$ingredients = $ingredientsMapper->findByMealId($meal->getId());
?>
<a href="index.php" class="btn btn-success btn-block mt-4">Tillbaka</a>
<h3 class="text-center mt-5"><?= $meal->getName(); ?></h3>
<p class="text-muted text-center">
    <?= $meal->getDescription(); ?>
</p>
<hr>
<h4>Du behöver:</h4>
<ul>
    <?php
    foreach ($ingredients as $ingredient) {
        echo '
        <li>' . $ingredient->getName() . ': ' . $ingredient->getAmount() . '
        ';
    }
    ?>
</ul>

<div class="row">
    <div class="col-sm-6">
        <a href="process/remove.php?id=<?= $meal->getId(); ?>" class="btn btn-danger btn-block">Ta bort</a>
    </div>
    <div class="col-sm-6">
        <a href="edit.php?id=<?= $meal->getId(); ?>" class="btn btn-secondary btn-block">Redigera</a>
    </div>
</div>

<?php
require_once 'src/pages/Bot.php';
?>